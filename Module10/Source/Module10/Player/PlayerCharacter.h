// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Player/PlayerStateBase.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/BoxComponent.h"

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class MODULE10_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	UFUNCTION()
	bool IsFacingRight();

	UFUNCTION()
	bool IsTurning();

	UFUNCTION()
	bool IsAttacking();

	UFUNCTION()
	void TurnAround();

	UFUNCTION()
	void Attack();

	UFUNCTION()
	void SetSwordParticleTemplate(UParticleSystem* particleTemplate);

	UPROPERTY(EditAnywhere)
	UDamageType* PlayerDamageType;

protected:

	DECLARE_DELEGATE_TwoParams(FForwardInputActionDelegate, EPlayerStateInputAction, EInputEvent);

	/*
	*
	*    PROTECTED PROPERTIES
	*
	*/

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UPlayerStateBase> initialState;

	UPROPERTY()
	UPlayerStateBase* currentPlayerState;

	UPROPERTY(EditAnywhere)
	UParticleSystemComponent* swordParticleSystem;

	UPROPERTY(EditAnywhere)
	UBoxComponent* swordTriggerBox;

	UPROPERTY()
	bool _facingRight = true;

	UPROPERTY(BlueprintReadWrite)
	bool _playingTurnAnimation = false;

	UPROPERTY(BlueprintReadWrite)
	bool _attacking = false;

	/*
	* 
	*    PROTECTED FUNCTIONS
	* 
	*/

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void PlayTurnAroundAnimation(bool turnToFaceRight);

	UFUNCTION(BlueprintImplementableEvent)
	void PlayAttackAnimation();

	UFUNCTION()
	void ForwardInputAction(EPlayerStateInputAction actionType, EInputEvent inputEvent);

	UFUNCTION()
	void ForwardInputAxis(EPlayerStateInputAxis axisType, float axisValue);

	UFUNCTION()
	void ForwardMovementInput(float value);

	UFUNCTION()
	void OnSwordOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
		, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
		, const FHitResult& SweepResult);

private:
	void ChangePlayerState(UPlayerStateBase* nextState);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
