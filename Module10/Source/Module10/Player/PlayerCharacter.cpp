// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerCharacter.h"
#include "GameFramework/DamageType.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	swordParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Sword Particles"));
	swordParticleSystem->SetupAttachment(GetMesh(), FName(TEXT("FX_weapon_base")));

	swordTriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Sword Trigger Box"));
	swordTriggerBox->SetupAttachment(GetMesh(), FName(TEXT("FX_weapon_base")));

	swordTriggerBox->SetCollisionProfileName(TEXT("Trigger"));

	PlayerDamageType = NewObject<UDamageType>(UDamageType::StaticClass(), UDamageType::StaticClass());
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	if (initialState != nullptr)
	{
		ChangePlayerState(NewObject<UPlayerStateBase>(UPlayerStateBase::StaticClass(), initialState));
	}
	swordTriggerBox->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::OnSwordOverlapBegin);
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (currentPlayerState != nullptr)
	{
		ChangePlayerState(currentPlayerState->Update(DeltaTime));
	}
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::ForwardMovementInput);

	PlayerInputComponent->BindAction<FForwardInputActionDelegate>("Jump", IE_Pressed, this
		, &APlayerCharacter::ForwardInputAction, EPlayerStateInputAction::PI_ACTION_JUMP, IE_Pressed);

	PlayerInputComponent->BindAction<FForwardInputActionDelegate>("Attack", IE_Pressed, this
		, &APlayerCharacter::ForwardInputAction, EPlayerStateInputAction::PI_ACTION_ATTACK, IE_Pressed);

}


void APlayerCharacter::ForwardInputAction(EPlayerStateInputAction actionType, EInputEvent inputEvent)
{
	if (currentPlayerState != nullptr)
	{
		ChangePlayerState(currentPlayerState->HandleAction(actionType, inputEvent));
	}
}

void APlayerCharacter::ForwardInputAxis(EPlayerStateInputAxis axisType, float axisValue)
{
	if (currentPlayerState != nullptr)
	{
		ChangePlayerState(currentPlayerState->HandleAxis(axisType, axisValue));
	}
}

void APlayerCharacter::ChangePlayerState(UPlayerStateBase* nextState)
{
	if (nextState != nullptr)
	{
		if (currentPlayerState != nullptr)
		{
			currentPlayerState->ExitState(this);
		}
		currentPlayerState = nextState;
		currentPlayerState->EnterState(this);
	}
}

void APlayerCharacter::ForwardMovementInput(float value)
{
	ForwardInputAxis(EPlayerStateInputAxis::PI_AXIS_MOVE_HORIZONTAL, value);
}

bool APlayerCharacter::IsFacingRight()
{
	return _facingRight;
}

bool APlayerCharacter::IsTurning()
{
	return _playingTurnAnimation;
}

void APlayerCharacter::TurnAround()
{
	_facingRight = !_facingRight;
	_playingTurnAnimation = true;
	PlayTurnAroundAnimation(_facingRight);
}

void APlayerCharacter::SetSwordParticleTemplate(UParticleSystem* particleTemplate)
{
	swordParticleSystem->SetTemplate(particleTemplate);
}

void APlayerCharacter::Attack()
{
	_attacking = true;
	PlayAttackAnimation();
}

bool APlayerCharacter::IsAttacking()
{
	return _attacking;
}

void APlayerCharacter::OnSwordOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
	, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
	, const FHitResult& SweepResult)
{
	if (_attacking && OtherActor != this)
	{
		GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Red, OtherActor->GetName());
	}
}
