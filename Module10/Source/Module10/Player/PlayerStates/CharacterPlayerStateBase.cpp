// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerStates/CharacterPlayerStateBase.h"
#include "Player/PlayerCharacter.h"

void UCharacterPlayerStateBase::EnterState(APawn* pawn)
{
	Super::EnterState(pawn);
	_controlledCharacter = Cast<APlayerCharacter>(_controlledPawn);
}