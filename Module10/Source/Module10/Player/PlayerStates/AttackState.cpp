// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerStates/AttackState.h"
#include "Player/PlayerCharacter.h"
#include "Player/PlayerStates/CharacterBaseMoveState.h"

void UAttackState::EnterState(APawn* player)
{
	Super::EnterState(player);

	_controlledCharacter->Attack();
}

UPlayerStateBase* UAttackState::Update(float dt)
{
	if (!_controlledCharacter->IsAttacking())
	{
		return NewObject<UPlayerStateBase>(UPlayerStateBase::StaticClass(), UCharacterBaseMoveState::StaticClass());
	}
	return nullptr;
}