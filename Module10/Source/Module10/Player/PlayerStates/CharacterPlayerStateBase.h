// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/PlayerStateBase.h"
#include "CharacterPlayerStateBase.generated.h"

class APlayerCharacter;

/**
 * 
 */
UCLASS()
class MODULE10_API UCharacterPlayerStateBase : public UPlayerStateBase
{
	GENERATED_BODY()
	
public:
	virtual void EnterState(APawn* player) override;

protected:
	APlayerCharacter* _controlledCharacter;
};
