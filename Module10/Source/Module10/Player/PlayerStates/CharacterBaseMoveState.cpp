// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerStates/CharacterBaseMoveState.h"
#include "Player/PlayerCharacter.h"

#include "Player/PlayerStates/AttackState.h"

UPlayerStateBase* UCharacterBaseMoveState::HandleAxis(EPlayerStateInputAxis axisType, float value)
{
	if (axisType == EPlayerStateInputAxis::PI_AXIS_MOVE_HORIZONTAL)
	{
		if (value > 0 && !_controlledCharacter->IsFacingRight() || value < 0 && _controlledCharacter->IsFacingRight())
		{
			_controlledCharacter->TurnAround();
		}

		if (!_controlledCharacter->IsTurning())
		{
			FVector direction = _controlledPawn->GetActorForwardVector();
			_controlledPawn->AddMovementInput(FVector(1, 0, 0), value);
		}
	}

	return nullptr;
}

UPlayerStateBase* UCharacterBaseMoveState::HandleAction(EPlayerStateInputAction actionType, EInputEvent inputEvent)
{
	if (actionType == EPlayerStateInputAction::PI_ACTION_JUMP && inputEvent == IE_Pressed)
	{
		_controlledCharacter->Jump();
	}
	else if (actionType == EPlayerStateInputAction::PI_ACTION_ATTACK && inputEvent == IE_Pressed)
	{
		return NewObject<UPlayerStateBase>(UPlayerStateBase::StaticClass(), UAttackState::StaticClass());
	}
	return nullptr;
}
