// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/PlayerStates/CharacterPlayerStateBase.h"
#include "CharacterBaseMoveState.generated.h"

/**
 * 
 */
UCLASS()
class MODULE10_API UCharacterBaseMoveState : public UCharacterPlayerStateBase
{
	GENERATED_BODY()
	
public:
	virtual UPlayerStateBase* HandleAxis(EPlayerStateInputAxis axisType, float axisValue) override;
	virtual UPlayerStateBase* HandleAction(EPlayerStateInputAction actionType, EInputEvent inputEvent) override;
};
