// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/PlayerStates/CharacterPlayerStateBase.h"
#include "AttackState.generated.h"

/**
 * 
 */
UCLASS()
class MODULE10_API UAttackState : public UCharacterPlayerStateBase
{
	GENERATED_BODY()
	
public:
	virtual void EnterState(APawn* player);
	virtual UPlayerStateBase* Update(float dt);
};
