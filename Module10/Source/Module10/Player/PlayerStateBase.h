// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Player/PlayerStateInputs.h"

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PlayerStateBase.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class MODULE10_API UPlayerStateBase : public UObject
{
	GENERATED_BODY()
	
public:
	virtual void EnterState(APawn* player);
	virtual void ExitState(APawn* player);
	
	virtual UPlayerStateBase* HandleAction(EPlayerStateInputAction actionType, EInputEvent inputEvent);
	virtual UPlayerStateBase* HandleAxis(EPlayerStateInputAxis axisType, float axisValue);
	virtual UPlayerStateBase* Update(float dt);

protected:
	APawn* _controlledPawn;
};
