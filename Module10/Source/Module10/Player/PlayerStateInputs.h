// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PlayerStateInputs.generated.h"

/**
 * 
 */
UENUM()
enum EPlayerStateInputAction
{
	PI_ACTION_JUMP,
	PI_ACTION_ATTACK
};

UENUM()
enum EPlayerStateInputAxis
{
	PI_AXIS_MOVE_HORIZONTAL,
	PI_AXIS_MOVE_VERTICAL
};