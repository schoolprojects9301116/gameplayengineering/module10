// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerStateBase.h"

void UPlayerStateBase::EnterState(APawn* pawn)
{
	_controlledPawn = pawn;
}

void UPlayerStateBase::ExitState(APawn* pawn)
{}

UPlayerStateBase* UPlayerStateBase::HandleAction(EPlayerStateInputAction actionType, EInputEvent inputEvent)
{
	return nullptr;
}

UPlayerStateBase* UPlayerStateBase::HandleAxis(EPlayerStateInputAxis axisType, float axisValue)
{
	return nullptr;
}

UPlayerStateBase* UPlayerStateBase::Update(float dt)
{
	return nullptr;
}
