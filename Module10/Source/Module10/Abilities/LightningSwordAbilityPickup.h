// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Particles/ParticleSystem.h"

#include "CoreMinimal.h"
#include "Abilities/AbilityPickup.h"
#include "LightningSwordAbilityPickup.generated.h"

/**
 * 
 */
UCLASS()
class MODULE10_API ALightningSwordAbilityPickup : public AAbilityPickup
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere)
	UParticleSystem* lightningParticleEffect;

public:

	virtual void ApplyAbility(APlayerCharacter* player);
};
