// Fill out your copyright notice in the Description page of Project Settings.


#include "Abilities/AbilityPickup.h"

#include "Player/PlayerCharacter.h"

// Sets default values
AAbilityPickup::AAbilityPickup()
{
	triggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	SetRootComponent(triggerBox);
	triggerBox->SetCollisionProfileName(TEXT("Trigger"));

	triggerBox->OnComponentBeginOverlap.AddDynamic(this, &AAbilityPickup::OnOverlapBegin);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(triggerBox);
}

// Called when the game starts or when spawned
void AAbilityPickup::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AAbilityPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAbilityPickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor
	, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep
	, const FHitResult& SweepResult)
{
	auto player = Cast<APlayerCharacter>(OtherActor);
	if (player != nullptr)
	{
		ApplyAbility(player);
	}
	Destroy();
}

void AAbilityPickup::ApplyAbility(APlayerCharacter* player)
{}