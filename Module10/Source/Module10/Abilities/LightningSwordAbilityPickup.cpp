// Fill out your copyright notice in the Description page of Project Settings.

#include "Abilities/LightningSwordAbilityPickup.h"
#include "Player/PlayerCharacter.h"
#include "Abilities/ElectricDamage.h"

void ALightningSwordAbilityPickup::ApplyAbility(APlayerCharacter* player)
{
	if (player != nullptr)
	{
		player->PlayerDamageType = NewObject<UDamageType>(UDamageType::StaticClass(), UElectricDamage::StaticClass());
		player->SetSwordParticleTemplate(lightningParticleEffect);
	}
}
