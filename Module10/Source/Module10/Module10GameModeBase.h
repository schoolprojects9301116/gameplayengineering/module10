// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Module10GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MODULE10_API AModule10GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
